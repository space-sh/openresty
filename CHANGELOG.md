# Space Module change log - openresty

## [1.6.2 - 2020-02-21]

* Update documentation


## [1.6.1 - 2018-04-23]

* Update PCRE mirror in Docker image

* Change CI configuration to only build on new versions


## [1.6.0 - 2018-04-02]

+ Add lua_system_constants lua module to Docker image


## [1.5.1 - 2018-03-28]

+ Add bcrypt lua module to Docker image


## [1.4.0 - 2017-10-01]

+ Add `-g` directives


## [1.3.0 - 2017-08-08]

+ Add `logrotate` alias in Dockerfile

+ Add `rmlogs` alias in Dockerfile


## [1.2.0 - 2017-06-11]

+ Add `runwithsecret` alias in Dockerfile

+ Add `copysecret` alias in Dockerfile

* Update documentation

* Change Arch Linux image for CI tests


## [1.1.1 - 2017-05-18]

* Rename expected local OUT variables

- Remove old `SUDO` behavior


## [1.1.0 - 2017-04-26]

+ Add auto completion

* Change `SPACE_SIGNATURE` to consider parameter constraints

* Update include and clone statements


## [1.0.1 - 2017-04-13]

* Update _PCRE_ mirror URL in Dockerfile


## [1.0.0 - 2017-04-12]

+ Initial version
