#
FROM registry.gitlab.com/space-sh/space:latest
MAINTAINER Blockie

# Version
ARG VERSION
LABEL VERSION="$VERSION"
ENV VERSION $VERSION

##############################################################################
# /SNATCHED (and slightly modified) FROM:
# https://github.com/openresty/docker-openresty                              #
# MAINTAINER Evan Wies <evan@neomantra.net>                                  #
#                                                                            #

# Docker Build Arguments
ARG RESTY_VERSION="1.11.2.2"
ARG RESTY_OPENSSL_VERSION="1.0.2j"
ARG RESTY_PCRE_VERSION="8.39"
ARG RESTY_J="1"

# These are not intended to be user-specified
ARG _RESTY_CONFIG_DEPS="--with-openssl=/tmp/openssl-${RESTY_OPENSSL_VERSION} --with-pcre=/tmp/pcre-${RESTY_PCRE_VERSION}"


# 1) Install apk dependencies
# 2) Download and untar OpenSSL, PCRE, and OpenResty
# 3) Build OpenResty
# 4) Cleanup

RUN \
    apk add --no-cache --virtual .build-deps \
        build-base \
        curl \
        gd-dev \
        geoip-dev \
        libxslt-dev \
        linux-headers \
        make \
        perl-dev \
        readline-dev \
        zlib-dev \
        autoconf gcc libc-dev lua5.1-dev openssl-dev \
    && apk add --no-cache \
        gd \
        geoip \
        libcrypto1.0 \
        libgcc \
        libxslt \
        zlib \
    && luarocks install bcrypt-ffi && cp /usr/local/lib/lua/5.1/libluabcrypt.so /usr/local/lib/ \
    && luarocks install lua_system_constants \
    && cd /tmp \
    && curl -fSL https://www.openssl.org/source/openssl-${RESTY_OPENSSL_VERSION}.tar.gz -o openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
    && tar xzf openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
    && curl -kfSL https://ftp.pcre.org/pub/pcre/pcre-${RESTY_PCRE_VERSION}.tar.gz -o pcre-${RESTY_PCRE_VERSION}.tar.gz \
    && tar xzf pcre-${RESTY_PCRE_VERSION}.tar.gz \
    && curl -fSL https://openresty.org/download/openresty-${RESTY_VERSION}.tar.gz -o openresty-${RESTY_VERSION}.tar.gz \
    && tar xzf openresty-${RESTY_VERSION}.tar.gz

ARG RESTY_CONFIG_OPTIONS="\
    --with-file-aio \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_geoip_module=dynamic \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_image_filter_module=dynamic \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_slice_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-http_v2_module \
    --with-http_xslt_module=dynamic \
    --with-ipv6 \
    --with-mail \
    --with-mail_ssl_module \
    --with-md5-asm \
    --with-pcre-jit \
    --with-sha1-asm \
    --with-stream \
    --with-stream_ssl_module \
    --with-threads \
    --without-http_ssi_module \
    --without-http_userid_module \
    --without-http_uwsgi_module \
    --without-http_scgi_module \
    --pid-path=/usr/local/openresty/nginx.pid \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --http-client-body-temp-path=/var/nginx/client_body_temp \
    --http-proxy-temp-path=/var/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/nginx/fastcgi_temp \
    "

RUN mkdir -p /var/nginx \
    && cd /tmp/openresty-${RESTY_VERSION} \
    && ./configure -j${RESTY_J} ${_RESTY_CONFIG_DEPS} ${RESTY_CONFIG_OPTIONS} \
    && make -j${RESTY_J} \
    && make -j${RESTY_J} install \
    && cd /tmp \
    && rm -rf \
        openssl-${RESTY_OPENSSL_VERSION} \
        openssl-${RESTY_OPENSSL_VERSION}.tar.gz \
        openresty-${RESTY_VERSION}.tar.gz openresty-${RESTY_VERSION} \
        pcre-${RESTY_PCRE_VERSION}.tar.gz pcre-${RESTY_PCRE_VERSION} \
    && apk del .build-deps

#ENTRYPOINT ["/usr/local/openresty/bin/openresty", "-g", "daemon off;"]

#                                                                            #
# /END OF SNATCH                                                             #
#                                                                            #
##############################################################################

RUN space -m openresty /_dep_install/
RUN space -m utils /_dep_install/
RUN space -m alias /_dep_install/

RUN space -m alias /add/ -- reload \
    "space -m openresty /reload/"

RUN space -m alias /add/ -- logrotate \
    "space -m openresty /logrotate/"

RUN space -m alias /add/ -- rmlogs \
    "space -m openresty /rmlogs/"

RUN space -m alias /add/ -- run \
    "space -m utils /waitforfile/ -- /server/nginx.conf 120 || { exit 1; } && \
     sleep 3; cd /server; space -m openresty /run/ -- /server/nginx.conf /server"

RUN space -m alias /add/ -- runwithsecret \
    "space -m utils /waitforfile/ -- /server/nginx.conf 120 || { exit 1; } && \
     sleep 3; space -m alias /run/ -- copysecret || { exit; } && cd /server && space -m openresty /run/ -- /server/nginx.conf /server;"

RUN space -m alias /add/ -- copysecret \
     "space -m utils /waitforfile/ -- /var/run/secrets/secrets.lua/secrets.lua 120 || { exit 2; } && mkdir -p /server/lua/secrets && cp /var/run/secrets/secrets.lua/secrets.lua /server/lua/secrets && chmod 644 /server/lua/secrets/secrets.lua"

CMD ["space",  "-m",  "alias", "/run/",  "--", "run"]
