#
# Copyright 2016-2017 Blockie AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

#======================
# OPENRESTY_DEP_INSTALL
#
# Verify that OpenResty is installed.
#
# Returns:
#   0: success
#   1: failure
#
#======================
OPENRESTY_DEP_INSTALL()
{
    SPACE_DEP="PRINT OPENRESTY_GET_OPENRESTY"

    # shellcheck disable=2039
    local out_openresty=''
    if ! OPENRESTY_GET_OPENRESTY; then
        PRINT "Cannot find openresty. You must install it or use a docker image which has it." "error"
        return 1
    fi
    PRINT "Found OpenResty" "ok"
}


# Disable warning about local keyword
# shellcheck disable=SC2039
# Disable warning about unused variables
# shellcheck disable=2034

#======================
# OPENRESTY_INSTALL
#
# Install OpenResty from source.
# NOTE: This is NOT implemented because it's easier to
# use an existing Docker image that is optimized in size.
#
# Parameters:
#   $1: version, OpenResty version to download and install.
#   $2: configure options (optional, has defaults)
#
# Returns:
#   0: success
#   1: failure
#
#======================
OPENRESTY_INSTALL()
{
    SPACE_SIGNATURE="version:1 [options]"
    SPACE_DEP="PRINT"

    local version="${1:-1.11.2.2}"
    shift

    local options="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    PRINT "Will not install OpenResty, because it is a heavy process with a lot of dependencies. Usually an existing Docker image is what you want." "error"
    return 1
}


# Disable warning about local keyword
# shellcheck disable=SC2039

#======================
# OPENRESTY_RUN
#
# Run OpenResty service
#
# Parameters:
#   $1: config file
#   $2: prefix
#
# Returns:
#   0: success
#   1: failure
#
#======================
OPENRESTY_RUN()
{
    # shellcheck disable=2034
    SPACE_SIGNATURE="[conf prefix directives]"
    SPACE_DEP="PRINT OPENRESTY_GET_OPENRESTY"

    local conf="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local prefix="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local out_openresty=''
    if ! OPENRESTY_GET_OPENRESTY; then
        PRINT "Cannot find openresty." "error"
        return 1
    fi

    PRINT "Start OpenResty." "info"

    ${out_openresty} ${conf:+-c $conf} ${prefix:+-p $prefix} "$@"
}


# Disable warning about local keyword
# shellcheck disable=SC2039

#======================
# OPENRESTY_RELOAD
#
# Signal openresty to reload.
#
# Returns:
#   0: success
#   1: failure
#
#======================
OPENRESTY_RELOAD()
{
    # shellcheck disable=2034
    SPACE_DEP="PRINT OPENRESTY_GET_OPENRESTY"

    local out_openresty=''
    if ! OPENRESTY_GET_OPENRESTY; then
        PRINT "Cannot find openresty." "error"
        return 1
    fi

    PRINT "Reload OpenResty." "info"

    ${out_openresty} -s reload
}

#======================
# OPENRESTY_LOGROTATE
#
# Rename active log files and then
# signal openresty USR1 to create new log files.
#
# Returns:
#   0: success
#   1: failure
#
#======================
OPENRESTY_LOGROTATE()
{
    # shellcheck disable=2034
    SPACE_SIGNATURE="logdir"
    SPACE_DEP="PRINT OPENRESTY_GET_OPENRESTY"

    local logdir="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local out_openresty=''
    if ! OPENRESTY_GET_OPENRESTY; then
        PRINT "Cannot find openresty." "error"
        return 1
    fi

    local counter=0
    local ts=
    ts=$(date +"%s")
    local file=
    for file in error.log access.log; do
        local filepath="${logdir}/${file}"
        if [ -f "${filepath}" ]; then
            PRINT "Rotate ${filepath} to ${file}.${ts}." "info"
            mv "${filepath}" "${filepath}.${ts}"
            counter=$((counter+1))
        else
            PRINT "Could not find ${filepath}." "warning"
        fi
    done

    if [ "${counter}" -gt 0 ]; then
        PRINT "Send signal USR1 to OpenResty." "info"
        ${out_openresty} -s reopen
    fi
}

#======================
# OPENRESTY_RMLOGS
#
# Delete all log files which have been rotated out.
#
# Returns:
#   0: success
#   1: failure
#
#======================
OPENRESTY_RMLOGS()
{
    # shellcheck disable=2034
    SPACE_SIGNATURE="logdir"
    # shellcheck disable=2034
    SPACE_DEP="PRINT OPENRESTY_GET_OPENRESTY"

    local logdir="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local out_openresty=''
    if ! OPENRESTY_GET_OPENRESTY; then
        PRINT "Cannot find openresty." "error"
        return 1
    fi

    local file=
    for file in error.log access.log; do
        local filepath="${logdir}/${file}"
        rm -f "${filepath}".*
    done

    PRINT "Deleted rotated out logs." "ok"
}

#=====================
# OPENRESTY_GET_OPENRESTY
#
# Find openresty program
#
# Expects:
#   out_openresty
#
# Returns:
#   0: success
#   1: failed to find
#
#=====================
OPENRESTY_GET_OPENRESTY()
{
    SPACE_DEP="PRINT"       # shellcheck disable=2034

    # shellcheck disable=2043
    for out_openresty in /usr/local/openresty/bin/openresty; do
        PRINT "Look for openresty: ${out_openresty}." "debug"
        if [ -f "${out_openresty}" ]; then
            PRINT "Found openresty: ${out_openresty}." "debug"
            return 0
        fi
    done
    out_openresty=''
    return 1
}
