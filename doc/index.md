---
modulename: OpenResty
title: Overview
giturl: gitlab.com/space-sh/openresty
editurl: /edit/master/doc/index.md
weight: 100
---
# OpenResty module

Manage and run an _OpenResty_ installation.  


## Dependencies

Modules:  
+ os  

External:  
+ openresty  
