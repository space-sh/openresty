---
modulename: OpenResty
title: /install/
giturl: gitlab.com/space-sh/openresty
editurl: /edit/master/doc/install.md
weight: 200
---
# OpenResty module: Install

Install _OpenResty_.  


## Example

Installing a specific version:
```sh
space -m openresty /install/ -- "1.11.2.2"
```

Exit status code is expected to be 0 on success.
