---
modulename: OpenResty
title: /logrotate/
giturl: gitlab.com/space-sh/openresty
editurl: /edit/master/doc/logrotate.md
weight: 200
---
# OpenResty module: Log rotate

Rotate access and error logs by renaming the files to include a timestamp of when the rotation occurred.  


## Example

Rotate logs contained in target directory:
```sh
space -m openresty /logrotate/ -- "/var/log/nginx"
```

Exit status code is expected to be 0 on success.
