---
modulename: OpenResty
title: /run/
giturl: gitlab.com/space-sh/openresty
editurl: /edit/master/doc/run.md
weight: 200
---
# OpenResty module: Run

Run _OpenResty_.  


## Example

Run using a particular configuration file:
```sh
space -m openresty /run/ -- "nginx.cfg"
```

Exit status code is expected to be 0 on success.
