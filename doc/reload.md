---
modulename: OpenResty
title: /reload/
giturl: gitlab.com/space-sh/openresty
editurl: /edit/master/doc/reload.md
weight: 200
---
# OpenResty module: Reload

Reload an existing _OpenResty_ service  


## Example

```sh
space -m openresty /reload/
```

Exit status code is expected to be 0 on success.
