# OpenResty module | [![build status](https://gitlab.com/space-sh/openresty/badges/master/pipeline.svg)](https://gitlab.com/space-sh/openresty/commits/master)

Manage and run an OpenResty installation.



## /install/
	Install OpenResty, not implemented.

	This does actually not install anything.
	It's a placeholder.
	


## /logrotate/
	Rotate error.log and access.log

	Rename log files to have '.ts' appended.
	ts being current unix timestamp.
	


## /reload/
	Reload OpenResty


## /rmlogs/
	Delete all rotated out logs

	Deletes all log files who has an
	appended '.ts' in their name.
	


## /run/
	Run OpenResty


# Functions 

## OPENRESTY\_DEP\_INSTALL()  
  
  
  
Verify that OpenResty is installed.  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## OPENRESTY\_INSTALL()  
  
  
  
Install OpenResty from source.  
NOTE: This is NOT implemented because it's easier to  
use an existing Docker image that is optimized in size.  
  
### Parameters:  
- $1: version, OpenResty version to download and install.  
- $2: configure options (optional, has defaults)  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## OPENRESTY\_RUN()  
  
  
  
Run OpenResty service  
  
### Parameters:  
- $1: config file  
- $2: prefix  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## OPENRESTY\_RELOAD()  
  
  
  
Signal openresty to reload.  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## OPENRESTY\_LOGROTATE()  
  
  
  
Rename active log files and then  
signal openresty USR1 to create new log files.  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## OPENRESTY\_RMLOGS()  
  
  
  
Delete all log files which have been rotated out.  
  
### Returns:  
- 0: success  
- 1: failure  
  
  
  
## OPENRESTY\_GET\_OPENRESTY()  
  
  
  
Find openresty program  
  
### Expects:  
- out\_openresty  
  
### Returns:  
- 0: success  
- 1: failed to find  
  
  
  
